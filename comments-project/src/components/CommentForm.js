import { useRef } from "react";

import "./CommentForm.css";

function CommentForm({ getComment }) {
  const commentName = useRef();
  const commentText = useRef();
  function getRandomID() {
    return Math.floor(Math.random() * 101);
  }

  const onSubmit = () => {
    getComment({
      id: getRandomID(),
      name: commentName.current.value,
      text: commentText.current.value,
    });
  };

  return (
    <div className="comment-form">
      <form
        onSubmit={(e) => {
          e.preventDefault();
          onSubmit();
        }}
      >
        <label>
          Name:
          <input
            type="text"
            name="name"
            ref={commentName}
            className="comment-input"
          />
        </label>
        <label>
          Text:
          <input
            type="text"
            name="text"
            ref={commentText}
            className="comment-input"
          />
        </label>
        <input
          type="submit"
          value="Отправить"
          className="button comment-submit"
        />
      </form>
    </div>
  );
}

export default CommentForm;
