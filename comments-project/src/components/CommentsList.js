import "./CommentsList.css";
import { useEffect, useState } from "react";

const CommentsList = ({ newComment }) => {
  const endpoint = `https://jordan.ashton.fashion/api/goods/30/comments`;

  const [showMore, setShowMore] = useState(false);
  const [displayButton, setDisplayButton] = useState(true);
  const [comments, setComments] = useState();

  const [page, setPage] = useState();
  const [nextPageUrl, setNextPageUrl] = useState();
  const [totalComments, setTotalComments] = useState();

  useEffect(() => {
    if (comments && comments.length >= totalComments) {
      setDisplayButton(false);
    }
  }, [comments, totalComments]);

  useEffect(() => {
    fetch(endpoint)
      .then((response) => response.json())
      .then((data) => {
        setComments(data.data);
        setPage(data.current_page);
        setNextPageUrl(data.next_page_url);
        setTotalComments(data.total);
      });
  }, [page, endpoint]);

  useEffect(() => {
    if (nextPageUrl && showMore) {
      fetch(nextPageUrl)
        .then((response) => response.json())
        .then((data) => {
          setComments(data.data.concat(comments));
        });
    }
  }, [showMore]);

  useEffect(() => {
    if (newComment) {
      setComments(comments.concat(newComment));
      setTotalComments(totalComments + 1);
    }
  }, [newComment]);

  const addMore = () => {
    setShowMore(true);
  };

  return (
    <div className="comments__wrapper">
      {comments ? (
        <div>
          <div className="comments__list">
            {comments.map((element) => (
              <div key={element.id} className="comments__list__item">
                <div className="comments__list__item-name">{element.name}</div>
                <div className="comments__list__item-text">{element.text}</div>
              </div>
            ))}
          </div>
          {displayButton ? (
            <button
              className="button comments__list__show-more-button"
              onClick={(e) => {
                e.preventDefault();
                addMore();
              }}
            >
              Show more...
            </button>
          ) : (
            <p>No more to show</p>
          )}
        </div>
      ) : (
        <div>Loading...</div>
      )}
    </div>
  );
};

export default CommentsList;
