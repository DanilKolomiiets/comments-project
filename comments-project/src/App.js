import { useState } from "react";
import "./App.css";
import CommentForm from "./components/CommentForm";
import CommentsList from "./components/CommentsList";

function App() {
  const [newComment, setNewComment] = useState();

  const getComment = (comment) => {
    setNewComment(comment);
  };

  return (
    <div className="App">
      <header className="App-header">
        <CommentForm getComment={getComment} />
        <CommentsList newComment={newComment} />
      </header>
    </div>
  );
}

export default App;
